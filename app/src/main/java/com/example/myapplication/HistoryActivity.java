package com.example.myapplication;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class HistoryActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        String string =getIntent().getStringExtra("intent1");
 String str2 = getIntent().getStringExtra("intent2");


        TextView textView = findViewById(R.id.textData);
        textView.setText(string+"\n"+"\n"+ str2);
    }
}
