package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    String history = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void Plus(View rrr) {
        EditText editTxt = findViewById(R.id.editTt);  // связываем переменную editTxt с элементом с id EditTt
        String text = editTxt.getText().toString(); // из элмента с id EditTt берем текст и переводим в строку

        EditText editTxt2 = findViewById(R.id.editText2);
        String text2 = editTxt2.getText().toString();

        double a = Double.valueOf(text);
        double b = Double.valueOf(text2);

        TextView textVw = findViewById(R.id.textV);

        String s = String.valueOf(a+b);
        textVw.setText(s);

        history = history + text + "+" + text2+ "=" +s+"\n";

    }

    public void onClickInt(View view){
        Intent intent = new Intent(this, HistoryActivity.class) ; // намерение перейти
        startActivity(intent);
        String string = "FIFA";
      String str2 = history;
        intent.putExtra("intent1", string);
        intent.putExtra("intent2", str2);

        startActivity(intent);
    }
}

